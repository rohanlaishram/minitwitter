/**
 * Created with IntelliJ IDEA.
 * User: udit
 * Date: 19/8/13
 * Time: 9:45 PM
 * To change this template use File | Settings | File Templates.
 */


var tabState = {
    TWEET: 0,
    FOLLOWER: 1,
    FOLLOWING: 2
};

var tweets_minTimestamp=0;
var follower_counter=0;
var following_counter=0;
var tab=tabState.TWEET;
var tweet_scrollEnded = false;
var follower_scrollEnded = false;
var following_scrollEnded = false;
var email="";
var tweet_counter=0;


var displayTweets = function (tweets) {
    var i =  0;
    if(tweet_counter !== 0 ) {
        while((i<tweets.length) && (tweets_minTimestamp <= tweets[i]['timestamp'])){
            i = i+1;
        }
    }
    for(; i< tweets.length; i++) {
        $("#content").append(formatTweet(tweets[i],profile_username,profile_name,pic));
    }
    $('.tweet-box').emoticons('/static/img/emoticons');
};

var formatUser = function(user) {
    var userHtml = "<div class='well'>"+
        "<span class='name'>"+user['name']+"</span>"+
        "<span class='username'> @"+user['username']+"</span>" +
        "<br/>"+
        "<a href=/users/"+user['username']+">View Profile"+"</a>" +
        "</div>";
    return userHtml;
};

var loadTweet = function() {
    $.ajax({
        type: "GET",
        url: '/fetchTweet?userid=' + profile_id + '&counter='+tweet_counter
    }).done(function(msg) {
            if(msg.length > 0){
                displayTweets(msg);
                if(tweet_counter === 0)
                    tweets_minTimestamp = msg[msg.length-1]['timestamp'];
                else
                    tweets_minTimestamp=Math.min(msg[msg.length-1]['timestamp'], tweets_minTimestamp);
                tweet_counter = tweet_counter + 1;
            }
            if(msg.length < 10)
                tweet_scrollEnded = true;
        });
};

var loadFollower = function() {
    $.ajax({
        type: "GET",
        url: '/fetchFollower?userid=' + profile_id + '&counter='+follower_counter
    }).done(function(msg) {
            if(msg.length > 0){
                for(var i=0; i<msg.length; i++) {
                    $("#content-follower").append(formatUser(msg[i]));
                }
                follower_counter=follower_counter+1;
            }
            if(msg.length < 50)
                follower_scrollEnded = true;
        });
};

var loadFollowing = function() {
    $.ajax({
        type: "GET",
        url: '/fetchFollowing?userid=' + profile_id + '&counter='+following_counter
    }).done(function(msg) {
            if(msg.length > 0){
                for(var i=0; i<msg.length; i++) {
                    $("#content-following").append(formatUser(msg[i]));
                }
                following_counter=following_counter+1;
            }
            if(msg.length < 50)
                following_scrollEnded = true;
        });
};

var showTweetTab = function() {
    if(tweets_minTimestamp===0)
        loadTweet();
    tab=tabState.TWEET;
    $("#content").show();
    $("#content-follower").hide();
    $("#content-following").hide();
};

var showFollowerTab = function() {
    if(follower_counter===0)
        loadFollower();
    tab=tabState.FOLLOWER;
    $("#content").hide();
    $("#content-follower").show();
    $("#content-following").hide();
};

var showFollowingTab = function() {
    if(following_counter===0)
        loadFollowing();
    tab=tabState.FOLLOWING;
    $("#content").hide();
    $("#content-follower").hide();
    $("#content-following").show();
};

var undo = function(){
    $(".alert").remove();
    $('#inputName').val('');
    $('#inputEmail').val('');
    $('#inputPassword').val('');
    $('#inputConfirmPassword').val('');
};

var formValidation = function (){
    var uname = document.updation.name;
    var passid = document.updation.password;
    var uemail = document.updation.email;
    var confirmid = document.updation.confirmPassword;
    email = uemail.value;

    if(nameValidation('#updateprofile',uname,true) &&  emailValidation('#updateprofile',uemail,true) && passwordValidation('#updateprofile',passid,true) && confirmPasswordValidation('#updateprofile',confirmid,true))
    {
        if(equals('#updateprofile',passid,confirmid))
        {
            return true;
        }
    }
    return false;
};

var postTweet = function() {
    var tweet_msg = $('#message').val();
    $('#message').val(addMentionsTags(tweet_msg));
    $.ajax({
        type: "POST",
        url: '/tweet',
        contentType : "application/json",
        data: JSON.stringify($('#update').serializeObject())
    }).done(function(msg) {
            if(isOwner == "true") {
                $("#content").prepend(formatTweet(msg,profile_username,profile_name,pic));
                $('.tweet-box').emoticons('/static/img/emoticons');
            }
            $("#message").val('');
            $('#counter').html('140');
            $('#postTweetModal').modal('toggle') ;
            $('div.mentions').remove();
        });
}

$(document).ready(function() {
    tweets_minTimestamp=0;
    follower_counter=0;
    following_counter=0;
    tab=tabState.TWEET;
    tweet_scrollEnded = false;
    follower_scrollEnded = false;
    following_scrollEnded = false;
    tweet_counter=0;

    showTweetTab();
    $('#tweet-btn').attr('disabled',true);
    $('.fileupload').fileupload();

    $('#updateProfilebtn').click(function(e){
        e.preventDefault();
        if(formValidation()){
            $.ajax({
                type: "POST",
                url: '/check_availability_email?email='+email
            }).done( function(){
                    $.ajax({
                        type: "POST",
                        url: '/update_user',
                        data: $('#updateprofile').serialize()
                    }).done( function(){
                            $('#editProfileModal').modal('toggle') ;
                            undo();
                        }) ;
                }).fail(function(){
                    prependItem('#updateprofile',"Email already exists.");
                }) ;
        }
    });

    $('#cancelUpdateProfile').click(function(e){
        e.preventDefault();
        $('#editProfileModal').modal('toggle');
        undo();
    });

    $('.follow').click(function(e) {
        $.ajax({
            type: "POST",
            url: '/toggle_follow',
            data: 'profile_id=' + profile_id + '&isfollowing=' + isFollowing
        }).done(function(msg) {
                window.location.reload(true);
            });
    });

    $('#profile_tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
        $(this).blur();
        var tabId = $(this).attr('id');
        if(tabId==="tweetTab")
            showTweetTab();
        else if(tabId==="followerTab")
            showFollowerTab();
        else if(tabId==="followingTab") {
            showFollowingTab();
        }
    });

    $('#uploadProfilePicbtn').click( function(){
        if($('#inputProfilePic').val() != ''){
            $("#profile_pic_form").submit();
        }
        $('#uploadProfilePicModal').modal('toggle');

        setTimeout(function(){
            $.ajax({
                type:"GET",
                url:'/get_userInfo?userid='+profile_id
            }).done(function(msg){
                    $('img').attr("src","/static/img/"+msg['pic']);
                    pic = msg['pic'];
                });
        },1000);
    });

    $('#profile_pic').hover(function(){
        if(isOwner == "true"){
            $('#editPhotobtn').show();
        }
    },function(){
        $('#editPhotobtn').hide();
    });

    $('#profile_pic').click(function(){
        $('#editPhotobtn').trigger('click');
    });

    $('#editPhotobtn').hover(function(){
        $('#editPhotobtn').show();
    },function(){
        $('#editPhotobtn').hide();
    }) ;

    $('#editPhotobtn').hide();

    $('#editPhotobtn').click( function(e){
        e.preventDefault();
        if(isOwner == "true"){
            $('#removebtn').trigger('click');
            $('#uploadProfilePicModal').modal('toggle');
        }
    });

    $('#cancelUploadProfilePic').click( function(){
        $('#uploadProfilePicModal').modal('toggle');
    });
});


$(document).endlessScroll({
        bottomPixels: 100,
        fireDelay: 10,
        //fireOnce: true,
        callback: function(i) {
            if(tab===tabState.TWEET && !tweet_scrollEnded)
                loadTweet();
            else if(tab==tabState.FOLLOWER && !follower_scrollEnded)
                loadFollower();
            else if(tab==tabState.FOLLOWING && !following_scrollEnded)
                loadFollowing();
        }
});