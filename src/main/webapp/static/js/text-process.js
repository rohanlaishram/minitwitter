var addMentionsTags = function(s) {
    $('textarea.mention').mentionsInput('getMentions', function(data) {
        for(var i = 0; i<data.length; i++) {
            s = s.replace(data[i].value, "@"+data[i].value);
        }
    });
    return s;
}

$(function(){
    $('#message').limita({
        limit: 140,
        id_result: "counter",
        alertClass: "alert"
    });

    $('#searchbar').submit(function(e){
        e.preventDefault();
        var search = document.searchbar.searchtext.value;
        window.location = '/users/'+search;
    });

    var throttledRequest = _.debounce(function(query, process){
        $.ajax({
            url: '/get_name_suggestion?prefix=' + query
            ,cache: false
            ,success: function(data){
                process( data );
            }
        });
    }, 300);


    $('#searchtext').typeahead({
        source: function ( query, process ) {
            throttledRequest( query, process );
        },
        matcher: function (item) {
            if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) > -1) {
                return true;
            }
        },
        highlighter: function (item) {
            var regex = new RegExp( '(' + this.query + ')', 'gi' );
            return item.replace( regex, "<strong>$1</strong>" );
        },
        updater: function ( selectedName ) {
            return selectedName;
        }
    });

    $('textarea.mention').mentionsInput({
        onDataRequest:function (mode, query, callback) {
            $.getJSON('/get_name_suggestion?prefix=' + query, function(responseData) {
                responseData = _.filter(responseData, function(item) { return item.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                callback.call(this, responseData);
            });
        }
    })
});