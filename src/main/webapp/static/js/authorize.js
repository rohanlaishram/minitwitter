/**
 * Created with IntelliJ IDEA.
 * User: udit
 * Date: 21/8/13
 * Time: 2:17 PM
 * To change this template use File | Settings | File Templates.
 */

var formValidation = function() {

    var uname = document.formlogin.username;
    var passid = document.formlogin.password;
    if(usernameValidation('#loginform',uname,false) && passwordValidation('#loginform',passid,false)){
        return true;
    }
    return false;
};

$(document).ready(function() {
    $('#loginform').prepend('');

    $('#loginform').submit( function(e){
        e.preventDefault();
        if(formValidation()){
            $.ajax({
                type: "POST",
                url: '/api/authorize',
                data: $(this).serialize()

            }).done(function() {
                    window.location = '/success';
                }).fail(function() {
                    $(".alert").remove();
                    $('#loginform').prepend('<div class="alert alert-error affix-top">Your credentials do not match</div>');
                });
        }
    });
});