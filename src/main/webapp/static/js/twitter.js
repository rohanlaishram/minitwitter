function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = month+' '+date+','+year+' '+hour+':'+min+':'+sec ;
    return time;
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var formatTweet = function(tweet,username,name,pic) {
    tweet['content'] = tweet['content'].replace(/@(\w+)/g,'<a href="/users/$1">@$1</a>');
    tweet['content'] = tweet['content'].replace( /(http:\/\/[^\s]+)/gi , '<a href="$1">$1</a>' );
    tweet['content'] = tweet['content'].replace( /(https:\/\/[^\s]+)/gi , '<a href="$1">$1</a>' );

    var tweetHtml =
    "<div class='tweet-box'>" +
        "<img src='/static/img/"+pic+"' class='img-circle thumbnail-user'> " +
        "<div class='triangle-border left' style='margin-left:95px'>"+
        "<a href='/users/"+username+"'>" +
        "<span class='name'>"+name+"</span>"+
        "</a>"+
        "<span class='username'> @"+username+"</span>" +
//        "<span class='timestamp'>"+"    "+timeConverter(tweet['timestamp'])+"</span>"
        "<span class='timestamp' data-livestamp='"+ tweet['timestamp']/1000+"'></span>"
        +"<br/>" +
        "<span class=tweetMsg>"+tweet['content']+"</span>"+
        "</div>" +
    "</div>";
    return tweetHtml;
}
