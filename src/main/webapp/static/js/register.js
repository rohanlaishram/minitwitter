var username = "", email="";

var formValidation = function () {
    var uname = document.registration.name;
    var passid = document.registration.password;
    var uemail = document.registration.email;
    var uuser = document.registration.username;
    var confirmid = document.registration.confirmPassword;
    username = uuser.value;
    email = uemail.value;

    if(nameValidation('#registerform',uname,false) &&  emailValidation('#registerform',uemail,false) && usernameValidation('#registerform',uuser) && passwordValidation('#registerform',passid,false) && confirmPasswordValidation('#registerform',confirmid,false) )
    {
        if(equals('#registerform',passid,confirmid))
        {
            return true;
        }
    }
    return false;
};


$(document).ready(function() {
    $('#registerform').submit( function(e){
        e.preventDefault();
        var flag = false;
        if(formValidation()){
            $.ajax({
                type: "POST",
                url : '/check_availability_user?username='+username
            }).done(function() {
                    $.ajax({
                        type: "POST",
                        url : '/check_availability_email?email='+email
                    }).done(function() {
                            $.ajax({
                                type: "POST",
                                url: '/add_user',
                                data: $('#registerform').serialize()
                            }).done( function(){
                                    window.location='/login';
                                });
                        }).fail(function() {
                            flag = false;
                            prependItem('#registerform',"Email already exists.")
                        });
                }).fail(function(){
                    prependItem('#registerform',"Username already taken.")
                });
        }
    });
});