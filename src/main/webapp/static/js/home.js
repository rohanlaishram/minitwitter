
var newsFeed_maxTid=0;
var newsFeed_minTid=0;
var scrollEnded = false;

var loadNewsFeed = function() {

    $.ajax({
        type: "GET",
        url: '/fetchNewsFeed?userid=' + userid + '&minTid='+newsFeed_minTid
    }).done(function(msg) {
            if(msg.length > 0){
                displayNewsFeed(msg);
                newsFeed_maxTid=Math.max(newsFeed_maxTid, msg[0]['tid']);
                newsFeed_minTid=msg[msg.length-1]['tid'];
            }
            if(msg.length < 10)
                scrollEnded = true;
        });

}

var displayNewsFeed = function (tweets) {
    for(var i=0; i< tweets.length; i++) {
        $("#content").append(formatTweet(tweets[i],tweets[i]['username'],tweets[i]['name'],tweets[i]['pic']));
    }
    $('.tweet-box').emoticons('/static/img/emoticons');
}

var reset = function() {
    $('#newsFeed-btn').hide();
    $('#top-tweet-button').hide();
}

var getSyncNewsFeed = function(){
    $.ajax({
        type: "GET",
        url: '/syncNewsFeed?userid=' + userid + '&maxTid='+newsFeed_maxTid
    }).done(function(msg) {
            for(var i=msg.length-1; i>=0; i--) {
                $("#content").prepend(formatTweet(msg[i],msg[i]['username'],msg[i]['name'],msg[i]['pic']));
            }
            $('.tweet-box').emoticons('/static/img/emoticons');
            newsFeed_maxTid=Math.max(newsFeed_maxTid, msg[0]['tid']);
            reset();
        });
}

var postTweet = function() {
    var tweet_msg = $('#message').val();
    $('#message').val(addMentionsTags(tweet_msg));
    $.ajax({
        type: "POST",
        url: '/tweet',
        contentType : "application/json",
        data: JSON.stringify($('#update').serializeObject())
    }).done(function(msg) {
            $('#message').val('');
            $('#counter').html(140);
            $('div.mentions').remove();
            getSyncNewsFeed();
        }) ;
}

var checkUnreadFeeds = function() {
    $.ajax({
        type: "GET",
        url: '/getUnreadFeedsCount?userid=' + userid
    }).done(function(msg) {

            if((msg.toString()).charAt(0) != '0'){
                $('#newsFeed-btn').show();
                $('#newsFeed-btn').html(msg+" new tweets");
//                $("#newsFeed-btn").attr('disabled',false);
                //$("#unreadNewsFeed").html(msg+" new tweets");
            }
        });
}


$(document).ready(function() {
    loading = false;
    loadNewsFeed();
    reset();
    setInterval(function(){
        checkUnreadFeeds();
    }, 120000);

    $('.get-mentions').click(function() {
        alert("test");
        $('textarea.mention').mentionsInput('getMentions', function(data) {
            alert(JSON.stringify(data));
        });
    }) ;

});

$(document).endlessScroll({
    bottomPixels: 100,
    fireDelay: 10,
    //fireOnce: true,
    callback: function(i) {
        if(!scrollEnded)
            loadNewsFeed();
    }
});