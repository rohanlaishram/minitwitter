(function($){
    $.fn.limita = function(options) {

        var defaults = {
            limit: 200,
            id_result: false,
            alertClass: false
        }

        var options = $.extend(defaults, options);

        return this.each(function() {

            var	characters = options.limit;

            if(options.id_result != false)
            {
                $("#"+options.id_result).append(characters);
            }

            $(this).keyup(function(){
                if($(this).val().length <= characters){
                    //$(this).val($(this).val().substr(0, characters));
                    $('#tweet-btn').attr('disabled',false);
                }

                if($(this).val().length > characters || $(this).val().length == 0){
                    //$(this).val($(this).val().substr(0, characters));
                    $('#tweet-btn').attr('disabled',true);
                }

                if(options.id_result != false)
                {

                    var remaining = characters - $(this).val().length;
                    $("#"+options.id_result).html(remaining);

                    if(remaining <= 10)
                    {
                        $("#"+options.id_result).css({"color":"red"});
                    }
                    else
                    {
                        $("#"+options.id_result).css({"color":"black"});
                    }
                }
            });

            $(this).keydown(function(){

            });
        });


    };
})(jQuery);