/**
 * Created with IntelliJ IDEA.
 * User: udit
 * Date: 29/7/13
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
var prependItem = function(item,msg){
    $(".alert").remove();
    var str = "<div class='alert alert-error affix-top'>"+msg+"</div>" ;
    $(item).prepend(str);
}
var usernameValidation = function(item,uname,isEmptyAllowed) {
    if(uname.value == "") {
        if(isEmptyAllowed){
            return true;}
        else{
            prependItem(item,"Username field cannot be empty") ;
            return false;
        }
    }
    var letters = /^[a-zA-Z0-9_-]{3,15}$/;
    if(!uname.value.match(letters))
    {
       prependItem(item,"Username contains invalid characters");
       return false;
    }
    return true;
};

var passwordValidation = function(item,passid,isEmptyAllowed) {
    if(passid.value == "")  {
        if(isEmptyAllowed){
            return true;}
        else{
            prependItem(item,"Password field cannot be empty'");
            return false;
        }
    }
    var passid_len = passid.value.length;
    if (passid_len >30 || passid_len < 3)
    {
        prependItem(item,"Password length should be between 3 to 30");
        passid.focus();
        return false;
    }
    return true;

};


var confirmPasswordValidation = function(item,passid,isEmptyAllowed) {
    if(passid.value == "")  {
        if(isEmptyAllowed){
            return true;  }
        else{
            prependItem(item,"Password field cannot be empty");
            return false;
        }
    }
    var passid_len = passid.value.length;
    if (passid_len >30 || passid_len < 3)
    {
        prependItem(item,"Password length should be between 3 to 30");
        passid.focus();
        return false;
    }
    return true;

};

var nameValidation = function(item,uname,isEmptyAllowed){
    if(uname.value == ""){
        if(isEmptyAllowed){
            return true;   }
        else{
            prependItem(item,"Name field cannot be empty");
            return false;
        }
    }
    var letters = /^[A-Za-z\s]+$/;
    if(!uname.value.match(letters))
    {
        prependItem(item,"Name must have alphabet characters only");
        uname.focus();
        return false;
    }
    return true;
};

var emailValidation = function (item,uemail,isEmptyAllowed){
    if(uemail.value == ""){
        if(isEmptyAllowed){
            return true; }
        else{
            prependItem(item,"Email field cannot be empty");
            return false;
        }
    }
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(!uemail.value.match(mailformat))
    {
        prependItem(item, "You have entered an invalid email address!");
        return false;
    }
    return true;
};

var equals = function(item,passid,confirmid){
    if(passid.value == confirmid.value)
        return true;
    prependItem(item,"Password & Confirm password should match.");
    return false;
};
