<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 30/7/13
  Time: 6:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/static/css/lib/bootstrap.css">
    <link rel="stylesheet" href="/static/css/lib/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="/static/css/login.css">
</head>
<body>
<div class="container">
    <form class="form-signin pull-right" id="loginform" name="formlogin" action="/api/authorize" method="POST">
        <h2 class="form-signin-heading">Authorize Api Access</h2>
        <input type="hidden" name="token" value="${token}" />
        <input type="text" class="input-block-level" name="username" id="username" placeholder="Username" >
        <input type="password" class="input-block-level" name="password" id="password" placeholder="Password" >
        <input class="btn btn-large btn-primary" type="submit" value="Authorize" />
    </form>

</div>
<jsp:include page="footer.jsp"/>
<script src="/static/js/lib/validations.js"></script>
<script src="/static/js/authorize.js"></script>
</body>
</html>