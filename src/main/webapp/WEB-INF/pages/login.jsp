<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="/static/css/lib/bootstrap.css">
    <link rel="stylesheet" href="/static/css/lib/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="/static/css/login.css">

</head>

<body>

<div class="container">
    <form class="form-signin pull-right" id="loginform" name="formlogin" method="POST">
        <h2 class="form-signin-heading">Sign in</h2>
        <input type="text" class="input-block-level" name="username" id="username" placeholder="Username" >
        <input type="password" class="input-block-level" name="password" id="password" placeholder="Password" >
        <label class="checkbox">
            <input type="checkbox" name="active" value="remember-me"> Remember me  </input>
        </label>
        <input class="btn btn-large btn-primary" type="submit" value="Sign in" />

        <a href="/register">Register</a>
    </form>

</div>
<jsp:include page="footer.jsp"/>
<script src="/static/js/lib/validations.js"></script>
<script src="/static/js/login.js"></script>

</body>
</html>
