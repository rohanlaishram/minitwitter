<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="/static/css/lib/bootstrap.css">
    <link rel="stylesheet" href="/static/css/lib/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="/static/css/login.css">
    <link rel="stylesheet" type="text/css" href="/static/css/register.css">
</head>

<body>

<div class="container">

    <form class="form-signin" id="registerform" name="registration" method="POST">
        <h2>
            <strong> New to MiniTwitter?</strong>
        </h2>
        <input type="text" class="input-block-level" name="name" id="inputName" placeholder="Your Name" />
        <input type="text" class="input-block-level" name="email" id="inputEmail" placeholder="Your Email" />
        <input type="text" class="input-block-level" name="username" id="inputUsername" placeholder="Username" >
        <input type="password" class="input-block-level" name="password" id="inputPassword" placeholder="Password" >
        <input type="password" class="input-block-level" name="confirmPassword" id="inputConfirmPassword" placeholder="Confirm Password" >
        <input class="btn btn-large btn-primary" type="submit" value="Sign Up" />

        <a href="/login">Login</a>
    </form>

</div>
<jsp:include page="footer.jsp" />
<script src="/static/js/lib/validations.js"></script>
<script src="/static/js/register.js"></script>
</body>
</html>
