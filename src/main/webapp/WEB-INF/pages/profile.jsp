    <!DOCTYPE html>

    <html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" language="java">
        <title>${profile_name}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <%@include file="styles.jsp" %>
        <link rel="stylesheet" type="text/css" href="/static/css/lib/bootstrap-fileupload.css">
        <link rel="stylesheet" type="text/css" href="/static/css/lib/bootstrap-fileupload.min.css">
        <link rel="stylesheet" type="text/css" href="/static/css/profile.css">
    </head>

    <body >
    <%@include file="bar.jsp"%>

    <div id="body" class="container-fluid">

        <div class="row-fluid">
            <div><%--hidden div to prevent first child aligning--%></div>


            <div class="span8 offset2 cover">
                <div class="picdiv profile-pic">
                    <%--<div class="editPhoto"--%>
                    <img id="profile_pic" class="pic profile-pic img-rounded" src="/static/img/${pic}" alt="Profile Pic">
                    <%--<div id="uploadProfilePicBtnModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="uploadProfilePicBtnModalLabel" aria-hidden="true">--%>
                        <p><a href="#" role="button" id="editPhotobtn" class="editPhoto btn btn-info " >Edit Photo</a></p>
                    <%--</div>--%>
                 </div>
                <h1 align="center" style="color:cornflowerblue"> ${profile_name}</h1>
                <h2 align="center" style="color:cornflowerblue"> @${profile_username}</h2>

                <% if(session.getAttribute("userid")!= null && Integer.parseInt(session.getAttribute("userid").toString())>0) {%>
                <% if (!request.getAttribute("profile_id").equals(session.getAttribute("userid"))) { %>

                <% if (request.getAttribute("isfollowing").equals("false")) { %>
                <input type="button" class="follow btn btn-primary" value="Follow">
                <% } else {%>
                <input type="button" class="follow btn btn-warning" value="Unfollow">
                <% } %>

                <% } else {%>
                <p><a href="#editProfileModal" role="button" class="btn btn-primary " data-toggle="modal">Edit Profile</a></p>
                <% } }%>

            </div>

            <div class="span8 offset2 display-area">
                  <div>
                        <div id="editProfileModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editProfileModalLabel" aria-hidden="true">
                            <form class="form-horizontal form-body" id="updateprofile" name="updation" method="POST">
                                <div class="control-group">
                                    <label class="control-label" for="inputName">Name</label>
                                    <div class="controls">
                                        <input type="text" id="inputName" name="name" placeholder="Name">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputEmail">Email</label>
                                    <div class="controls">
                                        <input type="text" id="inputEmail" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputPassword">Password</label>
                                    <div class="controls">
                                        <input type="password" id="inputPassword" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputConfirmPassword">Confirm Password</label>
                                    <div class="controls">
                                        <input type="password" id="inputConfirmPassword" name="confirmPassword" placeholder="Password">
                                    </div>
                                </div>

                            <div class="control-group">
                                <div class="controls" >
                                    <input id="cancelUpdateProfile" class="btn btn-danger" type="button" value="Cancel" />
                                    <input id="updateProfilebtn" type="submit" class="btn btn-success" value="Save Changes" />
                                </div>
                            </div>
                            </form>
                        </div>



                      <div id="uploadProfilePicModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="uploadProfilePicModalLabel" aria-hidden="true">

                          <form class="form-horizontal form-body" id="profile_pic_form" method="POST" enctype="multipart/form-data" action="/update_profilepic" target="hidden_iframe">
                              <div class="fileupload fileupload-new pagination-centered" data-provides="fileupload">
                              <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;"><img src="/static/img/${pic}" /></div>
                              <div>
                              <span class="btn btn-file">
                                  <span class="fileupload-new">Select image</span>
                                  <span class="fileupload-exists">Change</span>
                                  <input type="file" id="inputProfilePic" name="profile_pic_content" >
                              </span>
                              <%--<a href="#" id="removebtn" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>--%>
                              </div>
                              </div>
                                  <div class="control-group">
                                      <div class="controls" >
                                          <input id="cancelUploadProfilePic" class="btn btn-danger" type="button" value="Cancel" />
                                          <input id="uploadProfilePicbtn" type="submit" class="btn btn-success" value="Save Changes" />
                                      </div>
                                  </div>
                                  <iframe id="hidden_iframe" name="hidden_iframe"  height="0" width="0" frameborder="0"></iframe>
                          </form>

                      </div>

                        <div id="postTweetModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="postTweetModalLabel" aria-hidden="true">
                            <%@include file="tweetbox.jsp"%>
                        </div>




                        <ul id="profile_tab" class="nav nav-tabs">
                            <li class="active">
                                <a id="tweetTab" href="#" data-toggle="tab">Tweets (<span id="tweet-count">${tweetCount}</span>)</a>
                            </li>
                            <li>
                                <a id="followerTab" href="#" data-toggle="tab">Followers (<span id="follower-count">${followerCount}</span>)</a>
                            </li>
                            <li>
                                <a id="followingTab" href="#" data-toggle="tab"> Following (<span id="following-count">${followingCount}</span>)</a>
                            </li>
                        </ul>
                    </div>
                    <div id="content" class="long-list"></div>
                    <div id="content-follower" class="long-list"></div>
                    <div id="content-following" class="long-list"></div>
            </div><!--/span-->

        </div>
    </div><!--/.fluid-container-->
    <%@include file="footer.jsp" %>
    <%@include file="scripts.jsp" %>

    <script src="/static/js/profile.js"></script>
    <script src="/static/js/lib/validations.js"> </script>
    <script src="/static/js/lib/bootstrap.file-input.js" ></script>
    <script src="/static/js/lib/bootstrap-fileupload.js" ></script>
    <script src="/static/js/lib/bootstrap-fileupload.min.js" ></script>
    <script>
        var profile_username = '${profile_username}';
        var profile_name =' ${profile_name}';
        var profile_id = ${profile_id};
        var pic = '${pic}';
        var isFollowing = '${isfollowing}';
        var isOwner = '${isOwner}';
    </script>

    </body></html>