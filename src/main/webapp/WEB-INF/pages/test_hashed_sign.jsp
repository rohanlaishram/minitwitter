<%--
  Created by IntelliJ IDEA.
  User: rohan
  Date: 22/8/13
  Time: 3:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Test - Signature</title>
</head>
<body>
    <form name="token_details" action="test_hashed_sign" method="POST">
        <input name="token" type="text" placeholder="token"> </br/>
        <input name="token_secret" type="text" placeholder="secret"> </br/>
        <input name="nonce" type="text" placeholder="nonce"><br/>
        <input type="submit" value="Generate Signature">
    </form>
    <div>
        <ul>
            <li>token = ${token} </li>
            <li>token_secret = ${token_secret} </li>
            <li>nonce = ${nonce} </li>
            <li>timestamp = ${timestamp} </li>
            <li>signature = ${signature} </li>
        </ul>
    </div>
</body>
</html>