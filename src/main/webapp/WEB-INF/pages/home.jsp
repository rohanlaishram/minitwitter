<!DOCTYPE html>

<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" language="java">
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <%@include file="styles.jsp" %>
    <link rel="stylesheet" type="text/css" href="/static/css/home.css">
</head>

<body>

<%@include file="bar.jsp"%>
<div id="body" class="container-fluid">
    <div class="row-fluid">

        <div><%--hidden div to prevent first child aligning--%></div>

        <div  class="span8 offset2 display-area">
            <div class="span12">
                <div class="span3">
                    <img class="img-rounded profile-pic" src="/static/img/${pic}" alt="Profile Pic">
                </div>
                <div class="span9 pull-right">
                    <h3>${name}</h3>
                    <form id="update"  accept-charset="UTF-8" action="" method="POST">
                        <textarea class="span12 mention" id="message" name="content"
                            placeholder="Compose new tweet..." rows="3" style="max-height: 80px"></textarea>
                        <h6 id="counter" class="pull-right"></h6>
                        <button class="btn btn-info" id="tweet-btn" type="button" onclick="postTweet()" disabled="disabled">Post New Tweet</button>
                        <input type="hidden" name="authid" value=${userid}>
                    </form>
                </div>
            </div>

            <ul id="newsFeed_tab" class="nav nav-tabs">
                <li class="active">
                    <a id="newsFeedTab" href="#" data-toggle="tab">NewsFeed</a>
                </li>
            </ul>
            <div class="pagination-centered">
                <button type="button" class="btn btn-info" id="newsFeed-btn" style="text-align: center" onclick="getSyncNewsFeed()">
                0</button>
            </div>
            <div id="content" class="long-list"></div>
        </div>
    </div><!--/row-->
</div><!--/.fluid-container-->

<%@include file="footer.jsp" %>
<%@include file="scripts.jsp" %>
<script src="/static/js/home.js"></script>
<script>
    var userid = ${userid};
</script>

</body>
</html>