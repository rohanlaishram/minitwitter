
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Page not found &middot; MiniTwitter</title>
    <link rel="stylesheet" href="/static/css/error.css">
</head>
<body>

<div class="container">

    <h1>404</h1>
    <p><strong>Sorry, but the page you are looking for has not been found. Try checking the URL for errors.</strong></p>


    <div id="suggestions">
        <a href="/home">Home</a> &mdash;
        <a href="/profile">Profile</a>
    </div>
</div>
</body>
</html>
