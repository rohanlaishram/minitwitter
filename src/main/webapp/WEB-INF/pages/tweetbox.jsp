<div id="tweet-box" class="span12 well">
    <form id="update" accept-charset="UTF-8" action="" method="POST">
        <textarea class="span12 mention" id="message" name="content"
                  placeholder="Type in your tweet" rows="5" style="max-height: 100px"></textarea>
        <h6 id="counter" class="pull-right"></h6>
        <button class="btn btn-info" id="tweet-btn" type="button" onclick="postTweet()" disabled="disabled">Post New Tweet</button>
        <input type="hidden" name="authid" value=${userid}>
    </form>
</div>