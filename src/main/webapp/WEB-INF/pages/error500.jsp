
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Page not found &middot; MiniTwitter</title>
    <link rel="stylesheet" href="/static/css/error.css">
</head>
<body>

<div class="container">

    <h1>500</h1>
    <p><strong>Looks like something went wrong!</strong></p>

    <p>Usually this means that an unexpected error happened while processing your request. You can try refreshing the page. The problem may be temporary.</p>

    <div id="suggestions">
        <a href="/home">Home</a> &mdash;
        <a href="/profile">Profile</a>
    </div>
</div>
</body>
</html>
