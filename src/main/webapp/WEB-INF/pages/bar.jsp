<div class="navbar navbar-inverse navbar-fixed-top" name="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a href="/home" class="brand">MiniTwitter</a>
            <div class="nav-collapse collapse">
                <p class="navbar-text pull-right" style="margin-left: 10px">
                    Logged in as @<a href="/profile" class="navbar-link">${username}</a>
                </p>
                <ul class="nav">
                    <li id="home"><a href="/home">Home</a></li>
                    <li id="profile"><a  href="/profile">Profile</a></li>
                </ul>
                <p class="navbar-text pull-right" style="margin-left: 10px">
                    <a href="/logout" class="navbar-link">Logout</a>
                </p>
            </div><!--/.nav-collapse -->
            <form class="navbar-search pull-right" style="margin-left: 10px" id="searchbar" name="searchbar">
                <input id="searchtext" type="text" class="search-query" name="searchtext"  placeholder="Search" autocomplete="off" data-provide="typeahead" >
                <div class="icon-search"></div>
            </form>
            <button id="top-tweet-button" href="#postTweetModal" data-toggle="modal"   class="btn btn-info btn-small span1 pull-right"><i class="icon-edit"></i></button>
        </div>
    </div>
</div>
