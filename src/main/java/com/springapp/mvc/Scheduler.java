package com.springapp.mvc;

import com.springapp.mvc.Services.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 5/8/13
 * Time: 6:19 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class Scheduler {
    private final TokenService tokenService;

    @Autowired
    public Scheduler(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Scheduled(fixedRate = 60000 * 30)
    public void run() {
        tokenService.removeExpiredCookies();
        tokenService.removeExpiredRequestTokens();
        tokenService.removeExpiredAccessTokens();
        tokenService.removeExpiredNonce();
    }

}
