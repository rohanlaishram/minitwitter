package com.springapp.mvc.Repository;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 2/8/13
 * Time: 6:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class SerializeObject {
    public static byte[] bytesFromObjects(Object o) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        out = new ObjectOutputStream(bos);
        out.writeObject(o);
        byte[] objectBytes = bos.toByteArray();
        out.close();
        bos.close();
        return objectBytes;
    }

    public static Object objectFromBytes(byte[] objectBytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(objectBytes);
        ObjectInput in = null;
        in = new ObjectInputStream(bis);
        Object o = in.readObject();
        bis.close();
        in.close();
        return o;
    }
}
