package com.springapp.mvc.Repository;

import com.springapp.mvc.Model.Tweet;
import com.springapp.mvc.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutorService;

/**
 * Created with IntelliJ IDEA.
 * User: udit.aga
 * Date: 7/17/13
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class TweetRepository {
    private JdbcTemplate jdbcTemplate;
    private CacheManager cacheManager;
    private UserRepository userRepository;
    private ExecutorService executorService;
    private Integer maximumTid;

    @Autowired
    public void setBeans(JdbcTemplate jdbcTemplate, CacheManager cacheManager, UserRepository userRepository, ExecutorService executorService) {
        this.jdbcTemplate = jdbcTemplate;
        this.cacheManager = cacheManager;
        this.userRepository = userRepository;
        this.executorService = executorService;
        maximumTid = (Integer) jdbcTemplate.queryForObject("select max(tid) from twitter.tweets", Integer.class);
    }

    public Integer createTweet(String content, int authid) {
        final SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setTableName("twitter.tweets");
        insert.setColumnNames(Arrays.asList("content", "authid"));
        insert.setGeneratedKeyName("tid");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("content", content);
        params.put("authid", authid);
        Integer returnVal = ((Integer) insert.executeAndReturnKey(params));
        incrementFollowersUnreadFeedsCount(authid);
        cacheManager.del("tweetCount#" + authid);
        cacheManager.delAll("fetchTweet#" + authid + "#*");
        maximumTid = returnVal;
        return returnVal;
    }

    private void incrementFollowersUnreadFeedsCount(final Integer userid) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                List<User> users = userRepository.fetchFollowersByUserid(userid);
                for (User u : users) {
                    if (!cacheManager.exists("feedsCount#" + u.getUserid()))
                        initializeUnreadFeedsCount(u.getUserid());
                    cacheManager.incr("feedsCount#" + u.getUserid());
                }
            }
        });
    }

    private void initializeUnreadFeedsCount(Integer userid) {
        cacheManager.set("feedsCount#" + userid, 0 + "");
    }

    public Integer getUnreadFeedsCount(Integer userid) {
        if (!cacheManager.exists("feedsCount#" + userid))
            return 0;
        return Integer.parseInt(cacheManager.getString("feedsCount#" + userid));
    }

    public List<Tweet> fetchTweetByAuthid(int userid, Long mintimestamp, int limit) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from twitter.tweets where authid = ? and timestamp < ? order by timestamp desc limit ?",
                new Object[]{userid, new Timestamp(mintimestamp), limit});

        return fetchTweetsFromList(rows, false);
    }

    public List<Tweet> fetchTweetByAuthid(int userid, int counter, int limit) {
        int i = counter / 5, min;
        String key = "fetchTweet#" + userid + "#" + i;
        if (cacheManager.exists(key)) {
            System.out.println("inside cache");
            List<Tweet> tweets = (List<Tweet>) cacheManager.get(key);
            min = Math.min(tweets.size(), (counter + 1 - 5 * i) * limit);
            return tweets.subList((counter - 5 * i) * limit, min);
        } else {
            System.out.println("fetching from database");
            List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from twitter.tweets where authid = ? order by timestamp desc limit ? offset ?",
                    new Object[]{userid, limit * 5, limit * 5 * i});

            List<Tweet> tweets = fetchTweetsFromList(rows, false);
            min = Math.min(tweets.size(), (counter + 1 - 5 * i) * limit);
            cacheManager.set(key, tweets);
            return tweets.subList((counter - 5 * i) * limit, min);
        }

    }

    public List<Tweet> fetchNewsFeed(int userid, int mintid, int limit) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select tid,content,authid, temp.timestamp, username, name, pic from ((select tid, content, authid, timestamp" +
                " from twitter.tweets join twitter.following ON (twitter.following.userid =? and " +
                "fid = authid and followtimestamp <= timestamp and (unfollowtimestamp is null or unfollowtimestamp > timestamp) and tid < ? ) union select tid, content, authid, timestamp" +
                " from twitter.tweets where authid = ? and tid < ?) ORDER BY tid DESC LIMIT ?) as temp,twitter.users " +
                "where temp.authid = userid", new Object[]{userid, mintid, userid, mintid, limit});
        initializeUnreadFeedsCount(userid);
        return fetchTweetsFromList(rows, true);
    }

    private List<Tweet> fetchTweetsFromList(List<Map<String, Object>> rows, boolean flag) {
        List<Tweet> tweets = new ArrayList<Tweet>();

        for (Map<String, Object> row : rows) {
            Tweet tmpTweet = new Tweet();
            tmpTweet.setContent((String) row.get("content"));
            tmpTweet.setTimestamp(((Timestamp) row.get("timestamp")).getTime());
            tmpTweet.setAuthid((Integer) row.get("authid"));
            tmpTweet.setTid((Integer) row.get("tid"));
            if (flag) {
                tmpTweet.setName((String) row.get("name"));
                tmpTweet.setUsername((String) row.get("username"));
                tmpTweet.setPic((String) row.get("pic"));
            }
            tweets.add(tmpTweet);
        }

        return tweets;
    }

    public List<Tweet> syncNewsFeedByAuthid(int userid, int maxtid) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select tid,content,authid, temp.timestamp, username, name, pic from ((select tid, content, authid, timestamp" +
                " from twitter.tweets join twitter.following ON (twitter.following.userid =? and " +
                "fid = authid and followtimestamp <= timestamp and (unfollowtimestamp is null or unfollowtimestamp > timestamp) and tid > ? ) union select tid, content, authid, timestamp" +
                " from twitter.tweets where authid = ? and tid > ?) ORDER BY tid ) as temp,twitter.users " +
                "where temp.authid = userid", new Object[]{userid, maxtid, userid, maxtid});
        initializeUnreadFeedsCount(userid);
        return fetchTweetsFromList(rows, true);
    }

    public Integer findMaxTid() {
        return maximumTid;
    }

}