package com.springapp.mvc.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: rohan
 * Date: 12/8/13
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CacheManager {
    @Autowired
    JedisPool jedisPool;

    public void set(String key, Object o) {
        Jedis jedis = jedisPool.getResource();
        try {

            jedis.set(key.getBytes(), SerializeObject.bytesFromObjects(o));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        jedisPool.returnResource(jedis);
    }

    public void set(String key, String val) {
        Jedis jedis = jedisPool.getResource();
        jedis.set(key, val);
        jedisPool.returnResource(jedis);
    }

    public Object get(String key) {
        Jedis jedis = jedisPool.getResource();
        Object o = null;
        try {
            byte[] bytes = jedis.get(key.getBytes());
            o = SerializeObject.objectFromBytes(bytes);
        } catch (Exception e) {
        }
        jedisPool.returnResource(jedis);
        return o;
    }

    public String getString(String key) {
        Jedis jedis = jedisPool.getResource();
        String s = jedis.get(key);
        jedisPool.returnResource(jedis);
        return s;
    }

    public void del(String key) {
        Jedis jedis = jedisPool.getResource();
        jedis.del(key.getBytes());
        jedisPool.returnResource(jedis);
    }

    public void del(String... keys) {
        Jedis jedis = jedisPool.getResource();
        for (String k : keys) {
            jedis.del(k.getBytes());
        }
        jedisPool.returnResource(jedis);
    }

    public void delAll(String prefix) {
        Jedis jedis = jedisPool.getResource();
        Set<String> set = jedis.keys(prefix);
        for (String bte : set) {
            jedis.del(bte.getBytes());
        }

        jedisPool.returnResource(jedis);
    }

    public boolean exists(String key) {
        Jedis jedis = jedisPool.getResource();
        boolean result = jedis.exists(key.getBytes());
        jedisPool.returnResource(jedis);
        return result;
    }

    public void incr(String key) {
        Jedis jedis = jedisPool.getResource();
        jedis.incr(key.getBytes());
        jedisPool.returnResource(jedis);
    }
}
