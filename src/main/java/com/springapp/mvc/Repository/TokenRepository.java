package com.springapp.mvc.Repository;

import com.springapp.mvc.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 5/8/13
 * Time: 4:39 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class TokenRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public User findByUserName(String username) {
        User user = jdbcTemplate.queryForObject("Select * from twitter.users where username = ?", new Object[]{username}, new BeanPropertyRowMapper<User>(User.class));
        return user;
    }

    public void addRequestToken(int userid, String request_token) {
        final SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setTableName("twitter.apitemp");
        insert.setColumnNames(Arrays.asList("userid", "request_token"));
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userid", userid);
        params.put("request_token", request_token);
        insert.execute(params);
    }

    public void mapAccessToRequestToken(int userid, String request_token, String access_token) {
        try {
            jdbcTemplate.update("update twitter.apitemp set access_token = ? where userid=? and request_token = ?", new Object[]{access_token, userid, request_token});
        } catch (Exception e) {
        }
    }

    public void addAccessToken(String access_token, String access_token_secret, int userid) {
        final SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setTableName("twitter.apiaccess");
        insert.setColumnNames(Arrays.asList("access_token", "access_token_secret", "userid"));
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("access_token", access_token);
        params.put("access_token_secret", access_token_secret);
        params.put("userid", userid);
        insert.execute(params);
    }

    public String getAccessToken(String request_token) {
        try {
            return jdbcTemplate.queryForObject("select access_token from twitter.apitemp where request_token = ?", new Object[]{request_token}, String.class);
        } catch (Exception e) {
            return null;
        }
    }

    public String getAccessTokenSecret(String access_token) {
        try {
            return jdbcTemplate.queryForObject("select access_token_secret from twitter.apiaccess where access_token = ?", new Object[]{access_token}, String.class);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean verifyRequestToken(int userid, String token) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from twitter.apitemp where request_token=?", new Object[]{token});
        Map<String, Object> row = rows.get(0);
        if ((((Integer) row.get("userid")).intValue() != userid) || (((String) row.get("access_token")) != null))
            return false;
        return true;
    }

    public int getUseridFromAccessToken(String token) {
        try {
            List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from twitter.apiaccess where access_token=?", new Object[]{token});
            Map<String, Object> row = rows.get(0);
            return ((Integer) row.get("userid")).intValue();
        } catch (Exception e) {
            return -1;
        }
    }

    public boolean existsRequestToken(String requestToken) {
        return !(jdbcTemplate.queryForList("select * from twitter.apitemp where request_token=?", new Object[]{requestToken})).isEmpty();
    }

    public boolean existsAccessToken(String accessToken) {
        return !(jdbcTemplate.queryForList("select * from twitter.apiaccess where access_token=?", new Object[]{accessToken})).isEmpty();
    }

    public void addNonce(String access_token, Long timestamp, Integer nonce) {
        final SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setTableName("twitter.apinonce");
        insert.setColumnNames(Arrays.asList("access_token", "timestamp", "nonce"));
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("access_token", access_token);
        params.put("timestamp", timestamp);
        params.put("nonce", nonce);
        insert.execute(params);
    }

    public boolean existsNonce(String accessToken, Long timestamp, Integer nonce) {
        return !(jdbcTemplate.queryForList("select * from twitter.apinonce where access_token=? and timestamp=? and nonce=?", new Object[]{accessToken, timestamp, nonce})).isEmpty();
    }

    public void removeExpiredCookies() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            jdbcTemplate.update("delete from twitter.cookie where expiry < ?", new Object[]{timestamp});
        } catch (Exception e) {
        }
    }

    public void removeExpiredRequestTokens() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            jdbcTemplate.update("delete from twitter.apitemp where expiry < ?", new Object[]{timestamp});
        } catch (Exception e) {
        }
    }

    public void removeExpiredAccessTokens() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            jdbcTemplate.update("delete from twitter.apiaccess where expiry < ?", new Object[]{timestamp});
        } catch (Exception e) {
        }
    }

    public void removeExpiredNonce() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            jdbcTemplate.update("delete from twitter.apinonce where expiry < ?", new Object[]{timestamp});
        } catch (Exception e) {
        }
    }
}
