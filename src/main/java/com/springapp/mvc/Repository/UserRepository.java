package com.springapp.mvc.Repository;


import com.springapp.mvc.Model.Trie;
import com.springapp.mvc.Model.User;
import com.springapp.mvc.Services.SHA1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutorService;

@Repository
public class UserRepository {
    private JdbcTemplate jdbcTemplate;
    private CacheManager cacheManager;
    private ExecutorService executorService;

    private Trie usernames;
    private final int BLOCK_SIZE = 8;

    @Autowired
    public void setBeans(JdbcTemplate jdbcTemplate, CacheManager cacheManager, ExecutorService executorService) {
        this.jdbcTemplate = jdbcTemplate;
        this.cacheManager = cacheManager;
        this.executorService = executorService;
    }


    public Integer getUserid(String username) {
        String key = "getUserid#" + username;
        if (cacheManager.exists(key))
            return (Integer) cacheManager.get(key);

        Integer userid = jdbcTemplate.queryForObject("Select userid from twitter.users where username = ?", new Object[]{username}, Integer.class);
        cacheManager.set(key, userid);
        return userid;
    }

    public User findById(Integer userid) {
        String key = "findById#" + userid;
        if (cacheManager.exists(key))
            return (User) cacheManager.get(key);

        User user = jdbcTemplate.queryForObject("Select * from twitter.users where userid = ?", new Object[]{userid}, new BeanPropertyRowMapper<User>(User.class));
        cacheManager.set(key, user);
        return user;
    }

    public void addUser(final String username, String password, String name, String email) {
        final SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setTableName("twitter.users");
        insert.setColumnNames(Arrays.asList("username", "name", "password", "email"));
        insert.setGeneratedKeyName("userid");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", username);
        params.put("name", name);
        params.put("password", SHA1.generate(password));
        params.put("email", email);
        insert.executeAndReturnKey(params);
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                usernames.insertWord(username);
            }
        });

    }

    public void follow(Integer userid, Integer fid) {
        final SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setTableName("twitter.following");
        insert.setColumnNames(Arrays.asList("userid", "fid", "followtimestamp"));
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userid", userid);
        params.put("fid", fid);
        Timestamp timestamp = new Timestamp(new Date().getTime());
        params.put("followtimestamp", timestamp);
        try {
            insert.execute(params);
        } catch (Exception e) {
            jdbcTemplate.update("UPDATE twitter.following SET unfollowtimestamp = ? WHERE userid = ? and fid = ?", new Object[]{null, userid, fid});
        }
        cacheManager.del("isFollowing#" + userid + "#" + fid,
                "fetchFollowersByUserid#" + fid, "fetchFollowingsByUserid#" + userid,
                "followerCount#" + fid, "followingCount#" + userid);
    }

    public void unfollow(Integer userid, Integer fid) {
        jdbcTemplate.update("UPDATE twitter.following SET unfollowtimestamp = ? WHERE userid = ? and fid = ?", new Object[]{new Timestamp(new Date().getTime()), userid, fid});
        cacheManager.del("isFollowing#" + userid + "#" + fid,
                "fetchFollowersByUserid#" + fid, "fetchFollowingsByUserid#" + userid,
                "followerCount#" + fid, "followingCount#" + userid);
    }

    public Boolean isFollowing(Integer userid, Integer fid) {
        String key = "isFollowing#" + userid + "#" + fid;
        if (cacheManager.exists(key))
            return (Boolean) cacheManager.get(key);
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select unfollowtimestamp from twitter.following where userid = ? and fid = ? ", new Object[]{userid, fid});
        if (rows.isEmpty())
            return false;
        Timestamp unfollowTimestamp = (Timestamp) rows.get(0).get("unfollowtimestamp");
        Boolean isfollowing = false;
        if (unfollowTimestamp == null)
            isfollowing = true;

        cacheManager.set(key, isfollowing);
        return isfollowing;
    }

    public List<User> fetchFollowersByUserid(Integer userid) {
        String key = "fetchFollowersByUserid#" + userid;
        if (cacheManager.exists(key))
            return (List<User>) cacheManager.get(key);
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select twitter.users.userid, username, name, email from twitter.following, twitter.users where fid = ? and twitter.following.userid = twitter.users.userid and unfollowtimestamp is null", new Object[]{userid});
        List<User> users = fetchUsersFromList(rows, true);
        cacheManager.set(key, users);
        return users;
    }

    public List<User> fetchFollowingsByUserid(Integer userid) {
        String key = "fetchFollowingsByUserid#" + userid;
        if (cacheManager.exists(key))
            return (List<User>) cacheManager.get(key);
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select twitter.users.userid, username, name, email from twitter.following, twitter.users where twitter.following.userid = ? and twitter.users.userid = fid and unfollowtimestamp is null", new Object[]{userid});
        List<User> users = fetchUsersFromList(rows, true);
        cacheManager.set(key, users);
        return users;
    }

    private List<User> fetchUsersFromList(List<Map<String, Object>> rows, boolean flag) {
        List<User> users = new ArrayList<User>();
        for (Map<String, Object> row : rows) {
            User user = new User();
            if (flag) {
                user.setUserid((Integer) row.get("userid"));
                user.setName((String) row.get("name"));
                user.setEmail((String) row.get("email"));
            }
            user.setUsername((String) row.get("username"));
            users.add(user);
        }
        return users;
    }

    public void updateUser(int userid, Map<String, Object> params) {
        for (Object key : params.keySet()) {
            jdbcTemplate.update("UPDATE twitter.users SET " + key.toString() + " = ? WHERE userid = ?", new Object[]{params.get(key), userid});
        }
        cacheManager.del("findById#" + userid);
    }

    public void addCookie(int userid, String value) {
        final SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setTableName("twitter.cookie");
        insert.setColumnNames(Arrays.asList("cookievalue", "userid"));
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("cookievalue", value);
        params.put("userid", userid);
        insert.execute(params);
    }

    public int getCookieUserId(String value) {
        try {
            return (jdbcTemplate.queryForObject("Select userid from twitter.cookie where cookievalue = ?", new Object[]{value}, Integer.class)).intValue();
        } catch (Exception e) {
            return -1;
        }
    }

    public void removeCookie(String value) {
        try {
            jdbcTemplate.update("Delete from twitter.cookie where cookievalue = ?", new Object[]{value});
        } catch (Exception e) {
        }
    }

    public Integer tweetCount(Integer userid) {
        String key = "tweetCount#" + userid;
        if (cacheManager.exists(key))
            return (Integer) cacheManager.get(key);

        Integer tweetCount = ((Integer) jdbcTemplate.queryForObject("Select count(*) from twitter.tweets where authid = ?", new Object[]{userid}, Integer.class)).intValue();
        cacheManager.set(key, tweetCount);
        return tweetCount;
    }

    public Integer followerCount(Integer userid) {
        String key = "followerCount#" + userid;
        if (cacheManager.exists(key))
            return (Integer) cacheManager.get(key);

        Integer followerCount = ((Integer) jdbcTemplate.queryForObject("Select count(*) from twitter.following where fid = ? and unfollowtimestamp is null", new Object[]{userid}, Integer.class)).intValue();
        cacheManager.set(key, followerCount);
        return followerCount;
    }

    public Integer followingCount(Integer userid) {
        String key = "followingCount#" + userid;
        if (cacheManager.exists(key))
            return (Integer) cacheManager.get(key);

        Integer followingCount = ((Integer) jdbcTemplate.queryForObject("Select count(*) from twitter.following where userid = ? and unfollowtimestamp is null ", new Object[]{userid}, Integer.class)).intValue();
        cacheManager.set(key, followingCount);
        return followingCount;
    }

    public boolean checkUsernameAvailability(String username) {
        return (jdbcTemplate.queryForList("select * from twitter.users where username = ?", new Object[]{username})).isEmpty();
    }

    public boolean checkEmailAvailability(String email) {
        return (jdbcTemplate.queryForList("select * from twitter.users where email = ?", new Object[]{email})).isEmpty();
    }


    public List<User> getUsers() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select username from twitter.users where true");
        return fetchUsersFromList(rows, false);
    }

    public List<String> getUserNameSuggestion(int userid, String prefix) {
        List<String> users = usernames.matchPrefix(prefix), result;
        result = new ArrayList<String>();
        List<User> followingList = fetchFollowingsByUserid(userid);
        for (User user : followingList) {
            result.add(user.getUsername());
        }
        List<User> followersList = fetchFollowersByUserid(userid);
        for (User user : followersList) {
            if (!result.contains(user.getUsername()))
                result.add(user.getUsername());
        }
        result.retainAll(users);
        if (result.size() >= BLOCK_SIZE)
            return result.subList(0, BLOCK_SIZE);

        int size = result.size();
        for (String user : users) {
            if (!result.contains(user)) {
                result.add(user);
                size = size + 1;
                if (size == BLOCK_SIZE)
                    return result;
            }
        }
        return result;
    }

    @PostConstruct
    public void initializeTrie() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {

                    System.out.println("Loading trie...");
                    usernames = new Trie();
                    List<User> users = getUsers();
                    for (int i = 0; i < users.size(); i++) {
                        usernames.insertWord(users.get(i).getUsername());
                    }
                    System.out.println("Trie loaded");
                } catch (Exception e) {
                    e.printStackTrace();
                    usernames = new Trie();
                    System.out.println("Failed to load trie");
                }
            }
        });
    }

    public void updateProfilepic(int userid, String filename) {
        jdbcTemplate.update("update twitter.users set pic = ? where userid =?", new Object[]{filename, userid});
        cacheManager.del("findById#" + userid);
    }
}
