package com.springapp.mvc;

import com.springapp.mvc.Interceptor.ApiInterceptor;
import com.springapp.mvc.Interceptor.AuthenticationInterceptor;
import com.springapp.mvc.Services.TokenService;
import com.springapp.mvc.Services.UserService;
import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.beans.PropertyVetoException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Configuration
@ComponentScan(basePackages = "com.springapp.mvc")
@PropertySource(value = {"classpath:/dev/application.properties"})
@EnableWebMvc
@EnableTransactionManagement
public class AppConfig extends WebMvcConfigurerAdapter {

    @Bean
    public static JdbcTemplate jdbcTemplate(@Value("${db.server}") String serverName,
                                     @Value("${db.port}") String portNumber,
                                     @Value("${db.user}") String username,
                                     @Value("${db.database}") String databaseName,
                                     @Value("${db.password}") String password) throws PropertyVetoException {
        PGPoolingDataSource dataSource = new PGPoolingDataSource();
        dataSource.setServerName(serverName);
        dataSource.setPortNumber(Integer.parseInt(portNumber));
        dataSource.setDatabaseName(databaseName);

        dataSource.setUser(username);
        dataSource.setPassword(password);
        return new JdbcTemplate(dataSource);

    }

    private @Value("${redis.host-name}") String redisHostName;
    private @Value("${redis.port}") int redisPort;

    @Bean
    public JedisPool jedisPool() {
        return new JedisPool(new JedisPoolConfig(), redisHostName, redisPort);
    }

    private @Value("${executor.thread-pool-count}") int threadPoolCount;

    @Bean
    public ExecutorService executorService() {
        ExecutorService executorService = Executors.newFixedThreadPool(threadPoolCount);
        return executorService;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertiesConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new AuthenticationInterceptor(userService)).addPathPatterns("/home","/home/","/profile","/profile/","/fetchNewsFeed**", "/tweet**", "/syncNewsFeed**", "/update_user", "/toggle_follow", "/getUnreadFeedsCount**","/update_profilepic","/get_userInfo**");
        registry.addInterceptor(new ApiInterceptor(userService,tokenService)).addPathPatterns("/api/fetchNewsFeed**", "/api/tweet**");
    }
}
