package com.springapp.mvc.Controller;

import com.springapp.mvc.Services.SHA1;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: rohan
 * Date: 22/8/13
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TestController {
    @RequestMapping(value = "/test/test_hashed_sign", method = RequestMethod.GET)
    public String showHashedSign() {
        return "test_hashed_sign";
    }

    @RequestMapping(value = "/test/test_hashed_sign", method = RequestMethod.POST)
    public ModelAndView calculateSignature(@RequestParam("token") String token, @RequestParam("token_secret") String token_secret, @RequestParam("nonce") String nonce) {
        String timestamp = new Date().getTime() + "";
        String signature = SHA1.generate(token + timestamp + nonce + token_secret);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("token", token);
        modelAndView.addObject("token_secret", token_secret);
        modelAndView.addObject("nonce", nonce);
        modelAndView.addObject("timestamp", timestamp);
        modelAndView.addObject("signature", signature);
        modelAndView.setViewName("test_hashed_sign");
        return modelAndView;

    }
}
