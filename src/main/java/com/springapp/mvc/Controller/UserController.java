package com.springapp.mvc.Controller;

import com.springapp.mvc.Model.User;
import com.springapp.mvc.Services.UserService;
import com.springapp.mvc.Services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 15/7/13
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView showHome(HttpSession httpSession) {
        ModelAndView model = new ModelAndView();
        int userid = userService.getSessionUserId(httpSession);
        model.setViewName("home");
        User user = userService.findById(userid);
        model.addObject("name", user.getName());
        model.addObject("username", user.getUsername());
        model.addObject("pic", user.getPic());
        return model;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister() {
        return "register";
    }

    @RequestMapping(value = "/add_user", method = RequestMethod.POST)
    public ModelAndView addUser(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("name") String name, @RequestParam("email") String email, HttpServletResponse response) {
        ModelAndView model = new ModelAndView();
        RedirectView rv = new RedirectView("/login");
        rv.setExposeModelAttributes(false);
        model.setView(rv);
        try {
            userService.addUser(username, password, name, email);
            response.setStatus(HttpServletResponse.SC_CREATED);
            return model;
        } catch (ValidationService.ValidationException e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = "/check_availability_user", params = "username", method = RequestMethod.POST)
    public void checkUsernameAvailability(@RequestParam("username") String username, HttpServletResponse response) {
        if (userService.checkUsernameAvailability(username))
            response.setStatus(HttpServletResponse.SC_OK);
        else
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }


    @RequestMapping(value = "/check_availability_email", params = "email", method = RequestMethod.POST)
    public void checkEmailAvailability(@RequestParam("email") String email, HttpServletResponse response) {
        if (userService.checkEmailAvailability(email))
            response.setStatus(HttpServletResponse.SC_OK);
        else
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @RequestMapping(value = "/update_profilepic", method = RequestMethod.POST)
    public void uploadProfilePic(@RequestParam("profile_pic_content") MultipartFile profilepic, HttpSession httpSession, HttpServletResponse response) {
        try {
            userService.updateProfilePic(profilepic, httpSession);
        } catch (ValidationService.ValidationException e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/get_userInfo", params = "userid", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public User getProfilePic(@RequestParam("userid") int userid, HttpServletResponse response) {
        try {
            User user = userService.findById(userid);
            response.setStatus(HttpServletResponse.SC_OK);
            return user;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }


    @RequestMapping(value = "/update_user", method = RequestMethod.POST)
    public void updateUser(@RequestParam("name") String name, @RequestParam("email") String email, @RequestParam("password") String password, @RequestParam("confirmPassword") String confirmPassword, HttpSession httpSession, HttpServletResponse response) {
        try {
            userService.updateUser(name, email, password, confirmPassword, httpSession);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
        }
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView showMyProfile(HttpSession httpSession, HttpServletResponse response) {
        ModelAndView model = new ModelAndView();
        User user = userService.findById(userService.getSessionUserId(httpSession));
        model.addObject("isfollowing", "false");
        model.addObject("isOwner", true + "");
        return makeProfile(user, response, model);
    }

    @RequestMapping(value = "/users/{profile_username}", method = RequestMethod.GET)
    public ModelAndView showProfile(@PathVariable("profile_username") String profile_username, HttpSession httpSession, HttpServletResponse response) {
        ModelAndView model = new ModelAndView();
        User user = userService.findByUserName(profile_username);
        model.addObject("isfollowing", userService.isFollowing(userService.getSessionUserId(httpSession), user.getUserid()) + "");
        model.addObject("isOwner", false + "");
        return makeProfile(user, response, model);
    }

    private ModelAndView makeProfile(User user, HttpServletResponse response, ModelAndView model) {
        model.addObject("profile_id", user.getUserid() + "");
        model.addObject("profile_name", user.getName());
        model.addObject("profile_username", user.getUsername());
        model.addObject("pic", user.getPic());
        model.addObject("followerCount", userService.getFollowerCount(user.getUserid()));
        model.addObject("followingCount", userService.getFollowingCount(user.getUserid()));
        model.addObject("tweetCount", userService.getTweetCount(user.getUserid()));
        model.setViewName("/profile");
        response.setStatus(HttpServletResponse.SC_OK);
        return model;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showSearch() {
        return "search";
    }

    @RequestMapping(value = "/toggle_follow", method = RequestMethod.POST)
    public void addFollowing(@RequestParam("profile_id") int profile_id, @RequestParam("isfollowing") boolean isfollowing, HttpSession httpSession, HttpServletResponse response) {
        try {
            userService.toggleFollow(userService.getSessionUserId(httpSession), profile_id, isfollowing);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
        }
    }

    @RequestMapping(value = {"/fetchFollower"}, params = {"userid", "counter"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> fetchFollower(@RequestParam("userid") int userid, @RequestParam("counter") int counter, HttpServletResponse response) {
        try {
            List<User> followersList = userService.fetchFollowersByUserid(userid);
            response.setStatus(HttpServletResponse.SC_OK);
            return followersList.subList(counter * 50, Math.min(followersList.size(), (counter + 1) * 50));
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = {"/api/fetchFollower"}, params = {"userid"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> fetchFollowers(@RequestParam("userid") int userid, HttpServletResponse response) {
        try {
            List<User> followersList = userService.fetchFollowersByUserid(userid);
            response.setStatus(HttpServletResponse.SC_OK);
            return followersList;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = {"/fetchFollowing"}, params = {"userid", "counter"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> fetchFollowing(@RequestParam("userid") int userid, @RequestParam("counter") int counter, HttpServletResponse response) {
        try {
            List<User> followingsList = userService.fetchFollowingsByUserid(userid);
            response.setStatus(HttpServletResponse.SC_OK);
            return followingsList.subList(counter * 50, Math.min(followingsList.size(), (counter + 1) * 50));
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = {"/api/fetchFollowing"}, params = {"userid"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> fetchFollowings(@RequestParam("userid") int userid, HttpServletResponse response) {
        try {
            List<User> followingsList = userService.fetchFollowingsByUserid(userid);
            response.setStatus(HttpServletResponse.SC_OK);
            return followingsList;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = "/get_name_suggestion", params = "prefix", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getNameSuggestion(@RequestParam("prefix") String prefix, HttpSession httpSession) {
        int userid = userService.getSessionUserId(httpSession);
        List<String> result = userService.getUserNameSuggestion(userid, prefix);
        return result;
    }
}
