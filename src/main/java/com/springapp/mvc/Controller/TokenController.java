package com.springapp.mvc.Controller;

import com.springapp.mvc.Services.LoginService;
import com.springapp.mvc.Services.TokenService;
import com.springapp.mvc.Services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 30/7/13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class TokenController {
    private final LoginService loginService;
    private final TokenService tokenService;

    @Autowired
    public TokenController(LoginService loginService, TokenService tokenService) {
        this.loginService = loginService;
        this.tokenService = tokenService;
    }

    @RequestMapping(value = "/api/get_request_token", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> requestToken(@RequestParam("username") String username) {
        String requestToken = tokenService.grantRequestToken(username);
        String authorizationUrl = "https://localhost/api/authorize?token=" + requestToken;
        List<String> list = new ArrayList<String>();
        list.add(requestToken);
        list.add(authorizationUrl);
        return list;
    }

    @RequestMapping(value = "/api/authorize", params = "token", method = RequestMethod.GET)
    public String showAuthorize(@RequestParam("token") String token, ModelMap modelMap) {
        modelMap.addAttribute("token", token);
        return "authorize";
    }

    @RequestMapping(value = "/api/authorize", method = RequestMethod.POST)
    public void authorize(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("token") String token, HttpServletResponse response) {
        try {
            if (loginService.checkCredentials(username, password)) {
                if (!tokenService.verifyRequestToken(username, token))
                    throw new ValidationService.InvalidAuthorizationException();
                tokenService.grantAccessToken(username, token);
            } else throw new ValidationService.InvalidAuthorizationException();
        } catch (ValidationService.ValidationException e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
        } catch (ValidationService.InvalidAuthorizationException e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/api/get_access_token", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getAccessToken(@RequestParam("request_token") String requestToken) {
        String access_token = tokenService.getAccessToken(requestToken);
        String access_token_secret = tokenService.getAccessTokenSecret(access_token);
        List<String> list = new ArrayList<String>();
        list.add(access_token);
        list.add(access_token_secret);
        return list;
    }

    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public String success() {
        return "success";
    }
}
