package com.springapp.mvc.Controller;

import com.springapp.mvc.Model.User;
import com.springapp.mvc.Services.LoginService;
import com.springapp.mvc.Services.UserService;
import com.springapp.mvc.Services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 19/7/13
 * Time: 2:10 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@SessionAttributes({"userid", "username"})
public class SessionController {
    private final LoginService loginService;
    private final UserService userService;

    @Autowired
    public SessionController(LoginService loginService, UserService userService) {
        this.loginService = loginService;
        this.userService = userService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showRoot(HttpSession session) {
        ModelAndView model = new ModelAndView();
        int userid = userService.getSessionUserId(session);
        RedirectView rv;
        if (userid == 0) {
            rv = new RedirectView("/login");
        } else {
            User user = userService.findById(userid);
            model.addObject("userid", user.getUserid() + "");
            model.addObject("username", user.getUsername());
            model.addObject("pic", user.getPic());
            rv = new RedirectView("/home");
        }
        rv.setExposeModelAttributes(false);
        model.setView(rv);
        return model;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginScreen() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView validateLogin(@RequestParam("username") String username, @RequestParam("password") String password, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView();
        try {
            if (loginService.checkCredentials(username, password)) {
                User user = userService.findByUserName(username);
                model.addObject("userid", user.getUserid() + "");
                model.addObject("username", user.getUsername());
                model.addObject("pic", user.getPic());
                RedirectView rv = new RedirectView("/home");
                rv.setExposeModelAttributes(false);
                model.setView(rv);
                if (request.getParameter("active") != null) {
                    response.addCookie(userService.createCookie(user.getUserid()));
                }
                response.setStatus(HttpServletResponse.SC_OK);
                return model;
            } else {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return model;
            }
        } catch (ValidationService.ValidationException e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return model;
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView();
        model.addObject("userid", 0 + "");
        model.addObject("username", "");
        RedirectView rv = new RedirectView("/login");
        rv.setExposeModelAttributes(false);
        model.setView(rv);
        userService.removeCookie(request, response);
        response.setStatus(HttpServletResponse.SC_OK);
        return model;
    }
}
