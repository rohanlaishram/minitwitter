package com.springapp.mvc.Controller;

import com.springapp.mvc.Model.Tweet;
import com.springapp.mvc.Services.TweetService;
import com.springapp.mvc.Services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 17/7/13
 * Time: 6:36 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TweetController {

    private final TweetService tweetService;

    @Autowired
    public TweetController(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @RequestMapping(consumes = "application/json", value = {"/tweet", "/api/tweet"}, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Tweet createTweet(@RequestBody Tweet tweet, HttpServletRequest request, HttpServletResponse response) {
        if (tweet.getAuthid() != Integer.parseInt(request.getAttribute("userid").toString())) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        try {
            Tweet newTweet = tweetService.createTweet(tweet.getContent(), tweet.getAuthid());
            response.setStatus(HttpServletResponse.SC_CREATED);
            return newTweet;
        } catch (ValidationService.ValidationException e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = {"/fetchNewsFeed", "/api/fetchNewsFeed"}, params = {"userid", "minTid"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Tweet> fetchNewsFeed(@RequestParam("userid") int userid, @RequestParam("minTid") int minTid, HttpServletRequest request, HttpServletResponse response) {
        if (userid != Integer.parseInt(request.getAttribute("userid").toString())) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        try {
            List<Tweet> tweetList = tweetService.fetchNewsFeed(userid, minTid);
            response.setStatus(HttpServletResponse.SC_OK);
            return tweetList;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }


    @RequestMapping(value = {"/fetchTweet"}, params = {"userid", "counter"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Tweet> fetchTweets(@RequestParam("userid") int userid, @RequestParam("counter") int counter, HttpServletResponse response) {
        try {
            List<Tweet> tweetList = tweetService.fetchTweetByAuthid(userid, counter);
            response.setStatus(HttpServletResponse.SC_OK);
            return tweetList;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = {"/api/fetchTweet"}, params = {"userid", "mintimestamp"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Tweet> fetchTweets(@RequestParam("userid") int userid, @RequestParam("mintimestamp") Long mintimestamp, HttpServletResponse response) {
        try {
            if (mintimestamp == 0)
                mintimestamp = new Date().getTime() + 1;
            List<Tweet> tweetList = tweetService.fetchTweetByAuthid(userid, mintimestamp);
            response.setStatus(HttpServletResponse.SC_OK);
            return tweetList;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = {"/syncNewsFeed"}, params = {"userid", "maxTid"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Tweet> fetchSyncNewsFeed(@RequestParam("userid") int userid, @RequestParam("maxTid") int maxTid, HttpServletRequest request, HttpServletResponse response) {
        if (userid != Integer.parseInt(request.getAttribute("userid").toString())) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        try {
            List<Tweet> tweetList = tweetService.syncNewsFeedByAuthid(userid, maxTid);
            response.setStatus(HttpServletResponse.SC_OK);
            return tweetList;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return null;
        }
    }

    @RequestMapping(value = "/getUnreadFeedsCount", params = "userid", method = RequestMethod.GET)
    @ResponseBody
    public Integer getFeedsCount(@RequestParam("userid") int userid, HttpServletRequest request, HttpServletResponse response) {
        if (userid != Integer.parseInt(request.getAttribute("userid").toString())) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        try {
            return tweetService.getUnreadFeedsCount(userid);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }
}
