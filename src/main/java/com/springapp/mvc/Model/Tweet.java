package com.springapp.mvc.Model;


import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: udit.aga
 * Date: 7/17/13
 * Time: 11:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class Tweet implements Serializable {
    public Integer tid;
    public String content;
    public Long timestamp;
    public Integer authid;
    public String username;
    public String name;
    public String pic;

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {

        return username;
    }

    public String getName() {
        return name;
    }

    public Integer getTid() {
        return tid;
    }

    public String getContent() {
        return content;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public Integer getAuthid() {
        return authid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setAuthid(Integer authid) {
        this.authid = authid;
    }

}