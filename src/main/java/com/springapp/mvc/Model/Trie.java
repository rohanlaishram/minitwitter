package com.springapp.mvc.Model;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: udit
 * Date: 13/8/13
 * Time: 8:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class Trie implements Serializable {
    public Character letter;
    public Map<Character, Trie> links;
    boolean fullWord;

    public Trie(Character letter) {
        this.letter = letter;
        this.fullWord = false;
        this.links = new HashMap<Character, Trie>();
    }

    public Trie() {
        this.letter = new Character('\0');
        this.fullWord = false;
        this.links = new HashMap<Character, Trie>();
    }

    public void insertWord(String word) {
        int len = word.length();
        char[] letters = word.toCharArray();
        Trie currentNode = this;
        Character currentLetter;

        for (int i = 0; i < len; i++) {
            currentLetter = letters[i];
            if (currentNode.links.get(currentLetter) == null)
                currentNode.links.put(currentLetter, new Trie(letters[i]));
            currentNode = currentNode.links.get(currentLetter);
        }
        currentNode.fullWord = true;
    }

    public Trie find(String word) {
        char[] letters = word.toCharArray();
        int i, len = letters.length;
        Trie currentNode = this;
        Character currentLetter;
        for (i = 0; i < len; i++) {
            currentLetter = letters[i];
            if (currentNode.links.size() == 0)
                return null;
            currentNode = currentNode.links.get(currentLetter);
        }

        if ((i == len && currentNode == null)) return null;
        return currentNode;
    }

    public List<String> getTree(int level, char[] branch) {
        List<String> result = new ArrayList<String>();
        Iterator entries = this.links.entrySet().iterator();

        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            branch[level] = ((Character) entry.getKey()).charValue();
            Trie next = (Trie) entry.getValue();
            result.addAll(next.getTree(level + 1, branch));
        }

        if (this.fullWord) {
            result.add(new String(branch, 0, level));
        }

        return result;
    }

    public List<String> matchPrefix(String prefix) {
        Trie temp = find(prefix);
        List<String> result = new ArrayList<String>();
        if (temp == null)
            return result;
        char[] branch = new char[30];
        char[] letters = prefix.toCharArray();
        for (int i = 0; i < letters.length; i++)
            branch[i] = letters[i];
        result = temp.getTree(letters.length, branch);
        return result;
    }
}
