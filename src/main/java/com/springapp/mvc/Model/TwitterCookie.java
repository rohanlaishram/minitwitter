package com.springapp.mvc.Model;

import javax.servlet.http.Cookie;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 26/7/13
 * Time: 7:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class TwitterCookie extends Cookie {
    public static final int SECONDS_EXPIRY = 60 * 60 * 24;

    public TwitterCookie(String name, String value) {
        super(name, value);
        setMaxAge(SECONDS_EXPIRY);
    }
}
