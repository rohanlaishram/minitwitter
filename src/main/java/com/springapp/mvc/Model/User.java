package com.springapp.mvc.Model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 12/7/13
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class User implements Serializable {
    public Integer userid;
    public String name;
    public String password;
    public String email;
    public String username;
    public String pic;

    public User() {

    }

    public User(Integer userid, String name, String password, String email, String username, String pic) {
        this.userid = userid;
        this.name = name;
        this.password = password;
        this.email = email;
        this.username = username;
        this.pic = pic;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
