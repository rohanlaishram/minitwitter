package com.springapp.mvc.Interceptor;

import com.springapp.mvc.Services.UserService;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 12/7/13
 * Time: 1:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class AuthenticationInterceptor implements HandlerInterceptor {
    private UserService userService;

    public AuthenticationInterceptor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        int userid;
        try {
            userid = Integer.parseInt(httpServletRequest.getSession().getAttribute("userid").toString());
        } catch (Exception e) {
            userid = userService.checkCookie(httpServletRequest);
            if (userid > 0) {
                httpServletRequest.getSession().setAttribute("userid", userid + "");
                httpServletRequest.getSession().setAttribute("username", userService.findById(userid).getUsername() + "");
            }
        }

        if (userid <= 0) {
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            httpServletResponse.sendRedirect("/login");
            return false;
        }
        httpServletRequest.setAttribute("userid", userid);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
