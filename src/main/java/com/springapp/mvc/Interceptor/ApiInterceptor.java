package com.springapp.mvc.Interceptor;

import com.springapp.mvc.Model.User;
import com.springapp.mvc.Services.SHA1;
import com.springapp.mvc.Services.TokenService;
import com.springapp.mvc.Services.UserService;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 25/7/13
 * Time: 10:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ApiInterceptor implements HandlerInterceptor {

    private UserService userService;
    private TokenService tokenService;

    public ApiInterceptor(UserService userService, TokenService tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        String token = httpServletRequest.getHeader("token");
        String timestamp = httpServletRequest.getHeader("timestamp");
        String nonce = httpServletRequest.getHeader("nonce");
        String signature = httpServletRequest.getHeader("signature");
        User user = null;
        if (token == null || (user = checkCredentials(token, timestamp, nonce, signature)) == null) {
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }
        httpServletRequest.setAttribute("userid", user.getUserid());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse response, Object o, Exception e) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private User checkCredentials(String token, String timestamp, String nonce, String signature) {
        User user = null;
        try {
            if (!isTimeValid(Long.valueOf(timestamp)))
                return null;
            if (tokenService.existsNonce(token, Long.valueOf(timestamp), Integer.parseInt(nonce)))
                return null;
            tokenService.addNonce(token, Long.valueOf(timestamp), Integer.parseInt(nonce));
            int userid = tokenService.getUseridFromAccessToken(token);
            if (userid == -1)
                return null;
            user = userService.findById(userid);
            String token_secret = tokenService.getAccessTokenSecret(token);
            String _signature = SHA1.generate(token + timestamp + nonce + token_secret);
            if (!signature.equals(_signature))
                user = null;
        } catch (Exception e) {
            return null;
        }
        return user;
    }

    private boolean isTimeValid(Long timestamp) {
        Long currentTime = new Date().getTime();
        Long timeout = Long.valueOf(30 * 60 * 1000);
        if (Math.abs(currentTime - timestamp) > timeout)
            return false;
        return true;
    }
}
