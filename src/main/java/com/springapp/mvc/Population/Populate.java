package com.springapp.mvc.Population;

import com.google.gson.Gson;
import com.springapp.mvc.Model.Trie;
import com.springapp.mvc.Model.User;
import com.springapp.mvc.Repository.CacheManager;
import com.springapp.mvc.Repository.SerializeObject;
import com.springapp.mvc.Services.TweetService;
import com.springapp.mvc.Services.UserService;
import com.springapp.mvc.Services.ValidationService;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: udit
 * Date: 12/8/13
 * Time: 6:19 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class Populate {
    private JdbcTemplate jdbcTemplate;
    private UserService userService;
    private TweetService tweetService;
    private CacheManager cacheManager;

    @Autowired
    public Populate(JdbcTemplate jdbcTemplate, UserService userService, TweetService tweetService, CacheManager cacheManager){
        this.jdbcTemplate = jdbcTemplate;
        this.userService = userService;
        this.tweetService = tweetService;
        this.cacheManager = cacheManager;
    }

    @RequestMapping(value="/populate/following", method= RequestMethod.GET)
    public void populateFollowing() {
         String csvFile = "/home/rohan/edges.csv";
         BufferedReader br = null;
         String line="";
         String cvsSplitBy = ",";
         SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
         insert.setTableName("twitter.following");
         insert.setColumnNames(Arrays.asList("userid", "fid"));
         try{
             int count = 0;
             br = new BufferedReader(new FileReader(csvFile));
             System.out.println("Started");
             while ((line = br.readLine()) != null) {
                 String content[] = line.split(cvsSplitBy);
                 int userid = Integer.parseInt(content[0]);
                 int fid = Integer.parseInt(content[1]);
                 if(!(userid>1 && fid>1 && userid<646429 & fid<646429)) continue;
                 Map<String,Object> params = new HashMap<String,Object>();
                 params.put("userid",userid);
                 params.put("fid",fid);
                 try{
                    insert.execute(params);
                 }catch (Exception e){
                 }
                 count++;
                 if(count%10000==0) System.out.println(count);
             }
         }catch (Exception e){
             e.printStackTrace();
             if (br != null) {
                 try {
                     br.close();
                 } catch (IOException ex) {
                     e.printStackTrace();
                 }
             }
         }
        System.out.println("DONE!!!");
     }

    @RequestMapping(value="/populate/tweets", method= RequestMethod.GET)
    public void populateTweets() {
        String csvFile = "/media/udit/OSDisk/all.txt";
        BufferedReader br = null;
        String line="";
        String cvsSplitBy = ":",last="";
        int authid=0,i;
        try{
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String content[] = line.split(cvsSplitBy);
                if(!content[0].equals(last))         {
                    authid = userService.findByUserName(content[0]).getUserid();
                    last = content[0];
                }
                String str=content[1];
                for(i=2;i<content.length;i++){
                      str += ":";
                      str += content[i];
                }
                try{
                    tweetService.createTweet(str,authid);
                }catch (Exception e){}
                catch (ValidationService.ValidationException ex){}
            }
        }catch (Exception e){
            e.printStackTrace();
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequestMapping(value="/populate/trie", method= RequestMethod.GET)
    public void populateTrie() {
        List<Map<String,Object>> rows =  jdbcTemplate.queryForList("select username from twitter.users where true");
        Trie trie = new Trie();
        for(Map<String, Object> row : rows){
            trie.insertWord((String) row.get("username"));
        }
        cacheManager.set("trie-username", new Gson().toJson(trie));
    }

    @RequestMapping(value = "/populate/trie1", method = RequestMethod.GET)
    public void populateTrie1() {
        BufferedReader br = null;
        String path = "/home/rohan/Downloads/list";
        try {

            String sCurrentLine;
            Trie trie = new Trie();
            br = new BufferedReader(new FileReader(path));
            int i = 0;
            System.out.println("started");
            while ((sCurrentLine = br.readLine()) != null) {
                trie.insertWord(sCurrentLine.trim());
                i++;
                if(i%1000==0) System.out.println(i);
            }
            System.out.println("Trie insertion done");

            FileOutputStream fout = new FileOutputStream("/home/rohan/trie");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(trie);
            oos.close();
            System.out.println("Object written");


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
