package com.springapp.mvc.Services;

import com.springapp.mvc.Model.User;
import com.springapp.mvc.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 19/7/13
 * Time: 2:15 PM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class LoginService {
    private final UserRepository repository;

    @Autowired
    public LoginService(UserRepository repository) {
        this.repository = repository;
    }

    public boolean checkCredentials(String username, String password) throws ValidationService.ValidationException {
        if (!ValidationService.validateUsername(username) || !ValidationService.validatePassword(password)) {
            throw new ValidationService.ValidationException();
        }
        User user = repository.findById(repository.getUserid(username));
        if (user != null)
            return user.getPassword().equals(SHA1.generate(password)) ? true : false;
        return false;
    }
}
