package com.springapp.mvc.Services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 15/7/13
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class SHA1 {
    private static String salt = "SHA1 encryption";

    public static String generate(String s) {
        String hashedPass = null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            messageDigest.reset();
            messageDigest.update((s + salt).getBytes());
            byte[] output = messageDigest.digest();
            hashedPass = bytesToHex(output);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashedPass;
    }

    public static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }
}
