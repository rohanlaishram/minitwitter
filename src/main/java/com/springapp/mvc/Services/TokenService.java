package com.springapp.mvc.Services;

import com.springapp.mvc.Repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 5/8/13
 * Time: 4:16 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class TokenService {
    private final TokenRepository tokenRepository;

    @Autowired
    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }


    public boolean verifyRequestToken(String username, String token) {
        int userid = tokenRepository.findByUserName(username).getUserid();
        return tokenRepository.verifyRequestToken(userid, token);
    }

    public String grantRequestToken(String username) {
        int userid = tokenRepository.findByUserName(username).getUserid();
        String requestToken;
        do {
            requestToken = UUID.randomUUID().toString() + (new Date()).getTime();
        } while (tokenRepository.existsRequestToken(requestToken));
        tokenRepository.addRequestToken(userid, requestToken);
        return requestToken;
    }

    public void grantAccessToken(String username, String requestToken) {
        int userid = tokenRepository.findByUserName(username).getUserid();
        String accessToken;
        do {
            accessToken = UUID.randomUUID().toString() + (new Date()).getTime();
        } while (tokenRepository.existsAccessToken(accessToken));
        String accessTokenSecret = UUID.randomUUID().toString();
        tokenRepository.mapAccessToRequestToken(userid, requestToken, accessToken);
        tokenRepository.addAccessToken(accessToken, accessTokenSecret, userid);
    }

    public String getAccessToken(String requestToken) {
        return tokenRepository.getAccessToken(requestToken);
    }

    public String getAccessTokenSecret(String accessToken) {
        return tokenRepository.getAccessTokenSecret(accessToken);
    }

    public int getUseridFromAccessToken(String token) {
        return tokenRepository.getUseridFromAccessToken(token);
    }

    public void addNonce(String access_token, Long timestamp, Integer nonce) {
        tokenRepository.addNonce(access_token, timestamp, nonce);
    }

    public boolean existsNonce(String accessToken, Long timestamp, Integer nonce) {
        return tokenRepository.existsNonce(accessToken, timestamp, nonce);
    }

    public void removeExpiredCookies() {
        tokenRepository.removeExpiredCookies();
    }

    public void removeExpiredRequestTokens() {
        tokenRepository.removeExpiredRequestTokens();
    }

    public void removeExpiredAccessTokens() {
        tokenRepository.removeExpiredAccessTokens();
    }

    public void removeExpiredNonce() {
        tokenRepository.removeExpiredNonce();
    }
}
