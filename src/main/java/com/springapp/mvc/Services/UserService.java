package com.springapp.mvc.Services;

import com.springapp.mvc.Model.TwitterCookie;
import com.springapp.mvc.Model.User;
import com.springapp.mvc.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.springapp.mvc.Services.ValidationService.*;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 19/7/13
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserService {
    private final String profilePicPath = "/var/www/minitwitter/img/";
    private final UserRepository repository;


    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public int getUserid(String username) {
        return repository.getUserid(username);
    }

    public User findById(int userid) {
        return repository.findById(userid);
    }

    public User findByUserName(String username) {
        return repository.findById(repository.getUserid(username));
    }

    public boolean isFollowing(int userid, int fid) {
        return repository.isFollowing(userid, fid);
    }

    public void toggleFollow(int userid, int fid, boolean isFollowing) {
        if (isFollowing)
            repository.unfollow(userid, fid);
        else
            repository.follow(userid, fid);
    }

    public void addUser(String username, String password, String name, String email) throws ValidationService.ValidationException {
        if (!validateUsername(username) || !validatePassword(password) || !validateName(name) || !validateEmail(email))
            throw new ValidationService.ValidationException();
        else
            repository.addUser(username, password, name, email);
    }

    public List<User> fetchFollowersByUserid(int userid) {
        return repository.fetchFollowersByUserid(userid);
    }

    public List<User> fetchFollowingsByUserid(int userid) {
        return repository.fetchFollowingsByUserid(userid);
    }

    public int getSessionUserId(HttpSession httpSession) {
        int userid = 0;
        try {
            userid = Integer.parseInt(httpSession.getAttribute("userid").toString());
        } catch (Exception e) {
        }
        return userid;
    }

    public void updateUser(String name, String email, String password, String confirmPassword, HttpSession httpSession) {
        int userid = getSessionUserId(httpSession);
        Map<String, Object> params = new HashMap<String, Object>();
        if (validateName(name))
            params.put("name", name);
        if (validateEmail(email))
            params.put("email", email);
        if (validatePassword(password) && validatePassword(confirmPassword) && password.equals(confirmPassword))
            params.put("password", SHA1.generate(password));

        repository.updateUser(userid, params);
    }

    public void updateProfilePic(MultipartFile multipartFile, HttpSession httpSession) throws ValidationService.ValidationException, IOException {
        if (!validateProfilePic(multipartFile))
            throw new ValidationService.ValidationException();
        String filename = getSessionUserId(httpSession) + "_" + UUID.randomUUID().toString() + ".jpg";
        File destination = new File(profilePicPath + filename);
        multipartFile.transferTo(destination);
        try {
            String pic = findById(getSessionUserId(httpSession)).getPic();
            File file = new File(profilePicPath + pic);

            repository.updateProfilepic(getSessionUserId(httpSession), filename);
            file.delete();
        } catch (Exception e) {
        }

    }

    public TwitterCookie createCookie(int userid) {
        String cookievalue = UUID.randomUUID().toString() + "#" + SHA1.generate(userid + "");
        repository.addCookie(userid, cookievalue);
        return new TwitterCookie("rememberme", cookievalue);
    }

    public void removeCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = getCookie(request.getCookies(), "rememberme");
        if (cookie == null)
            return;
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        repository.removeCookie(cookie.getValue());
    }

    public int getCookieUserId(String value) {
        return repository.getCookieUserId(value);
    }

    public Cookie getCookie(Cookie[] cookies, String cookieName) {
        for (int i = 0; i < cookies.length; i++) {
            Cookie cookie = cookies[i];
            if (cookieName.equals(cookie.getName()))
                return cookie;
        }
        return null;
    }

    public int checkCookie(HttpServletRequest request) {
        try {
            Cookie[] cookies = request.getCookies();
            Cookie cookie = getCookie(cookies, "rememberme");
            int userid = getCookieUserId(cookie.getValue());
            return userid;
        } catch (Exception e) {
            return -1;
        }
    }

    public boolean checkUsernameAvailability(String username) {
        return repository.checkUsernameAvailability(username);
    }

    public boolean checkEmailAvailability(String email) {
        return repository.checkEmailAvailability(email);
    }

    public List<String> getUserNameSuggestion(int userid, String prefix) {
        List<String> result = repository.getUserNameSuggestion(userid, prefix);
        return result;
    }

    public int getTweetCount(int userid) {
        return repository.tweetCount(userid);
    }

    public int getFollowerCount(int userid) {
        return repository.followerCount(userid);
    }

    public int getFollowingCount(int userid) {
        return repository.followingCount(userid);
    }
}
