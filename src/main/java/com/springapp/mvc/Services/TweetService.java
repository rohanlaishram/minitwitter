package com.springapp.mvc.Services;

import com.springapp.mvc.Model.Tweet;
import com.springapp.mvc.Repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: root
 * Date: 19/7/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class TweetService {
    private final TweetRepository tweetRepository;

    @Autowired
    public TweetService(TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
    }

    public Tweet createTweet(String content, int authid) throws ValidationService.ValidationException {
        if (!ValidationService.validateContent(content))
            throw new ValidationService.ValidationException();
        else {
            content = content.replace("<", "&lt;");
            int tid = tweetRepository.createTweet(content, authid);
            Tweet tweet = new Tweet();
            tweet.setTid(tid);
            tweet.setContent(content);
            tweet.setTimestamp(new Date().getTime());
            tweet.setAuthid(authid);
            return tweet;
        }
    }

    public List<Tweet> fetchTweetByAuthid(int userid, Long mintimestamp) {
        return tweetRepository.fetchTweetByAuthid(userid, mintimestamp, 10);
    }

    public List<Tweet> fetchTweetByAuthid(int userid, int counter) {
        return tweetRepository.fetchTweetByAuthid(userid, counter, 10);
    }

    public List<Tweet> fetchNewsFeed(int userid, int minTid) {
        if (minTid == 0)
            minTid = findMaxTid() + 1;
        return tweetRepository.fetchNewsFeed(userid, minTid, 10);
    }

    public int getUnreadFeedsCount(int userid) {
        return tweetRepository.getUnreadFeedsCount(userid);
    }

    public List<Tweet> syncNewsFeedByAuthid(int userid, int maxTid) {
        return tweetRepository.syncNewsFeedByAuthid(userid, maxTid);
    }

    public int findMaxTid() {
        return tweetRepository.findMaxTid();
    }
}
