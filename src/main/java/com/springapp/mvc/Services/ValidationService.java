package com.springapp.mvc.Services;

import org.springframework.web.multipart.MultipartFile;

import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: udit
 * Date: 29/7/13
 * Time: 3:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class ValidationService {

    public static boolean validateUsername(String username) {
        String regex = "^[a-zA-Z0-9_-]{3,15}$";
        boolean ok = Pattern.matches(regex, username);
        return ok;
    }

    public static boolean validatePassword(String password) {
        if (password.length() > 30 || password.length() < 3) return false;
        return true;
    }

    public static boolean validateEmail(String email) {
        if (email.equals("")) return false;
        String regex = "\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";
        boolean ok = Pattern.matches(regex, email);
        return ok;
    }

    public static boolean validateName(String name) {
        if (name.equals("")) return false;
        String regex = "^[A-Za-z\\s]+$";
        boolean ok = Pattern.matches(regex, name);
        return ok;
    }

    public static boolean validateContent(String content) {
        if (content.equals("") || content.length() > 140) return false;
        return true;
    }

    public static boolean validateProfilePic(MultipartFile multipartFile) {
        if (multipartFile.isEmpty()) return false;
        if (multipartFile.getSize() > 204800) return false;
//        if(!multipartFile.getContentType().equals("image/jpeg")) return false;
        return true;
    }

    public static class ValidationException extends Throwable {
    }

    public static class InvalidAuthorizationException extends Throwable {
    }

    public static class InvalidTokenRequest extends Throwable {
    }
}
